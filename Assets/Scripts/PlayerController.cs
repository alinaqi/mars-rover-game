﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

	public float speed;
	public float tilt;
	public Boundary boundary;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;

	private Rigidbody rb;
	private float myTime = 0.0F;
	private float nextFire;

	private int[] countPickup;

	void Start() {
		rb = GetComponent<Rigidbody>();
		countPickup = new int[4]; // [food, fuel, tools, oxygen]
		countPickup[0] = 0; countPickup[1] = 0; countPickup[2] = 0; countPickup[3] = 0;
	}

	void Update ()
	{
		myTime = myTime + Time.deltaTime;

		if (Input.GetKeyDown("space") && myTime > nextFire) {          // originally Input.GetButton("Fire1")
			nextFire = myTime + fireRate;
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
			AudioSource audio = GetComponent<AudioSource>();
			audio.Play ();
		}
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement * speed;

		rb.position = new Vector3 
		(
				Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax), 
			0.0f, 
				Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
		);

		rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);

	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag ("Food") || other.gameObject.CompareTag ("Fuel") || other.gameObject.CompareTag ("Tools") || other.gameObject.CompareTag ("Oxygen")) {
			other.gameObject.SetActive (false);

			if (other.gameObject.CompareTag ("Food")) {
				countPickup [0]++;
			} else if (other.gameObject.CompareTag ("Fuel")) {
				countPickup [1]++;
			} else if (other.gameObject.CompareTag ("Tools")) {
				countPickup [2]++;
			} else if (other.gameObject.CompareTag ("Oxygen")) {
				countPickup [3]++;
			} else {
				// should not be here
				Debug.Log ("Error: Counting something that shouldn't be counted!");
			}

			Debug.Log ("[" + countPickup [0] + ", " + countPickup [1] + ", " + countPickup [2] + ", " + countPickup [3] + "]");
		}
	}
}
