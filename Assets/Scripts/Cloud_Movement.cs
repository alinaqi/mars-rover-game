﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud_Movement : MonoBehaviour {

	private float min=2f;
	private float max=3f;

	void Start () {
		min=transform.position.x;
		max=transform.position.x+3;
	}

	// Update is called once per frame
	void Update () {
		transform.position =new Vector3(Mathf.PingPong(Time.time,max-min)+min, transform.position.y, transform.position.z) ;
	} 

}
