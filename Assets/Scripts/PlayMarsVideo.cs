﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(AudioSource))]

public class PlayMarsVideo : MonoBehaviour {

	public MovieTexture movie;
	public string sceneName;
	private AudioSource audio;

	void Start () {
		GetComponent<RawImage> ().texture = movie as MovieTexture;
		audio = GetComponent<AudioSource> ();
		audio.clip = movie.audioClip;
		movie.Play ();
		audio.Play ();
	}
	
	void Update () {
		if (!movie.isPlaying) {
			Application.LoadLevel (sceneName);
		}

		if (Input.GetKeyDown (KeyCode.Space) && movie.isPlaying) {
			movie.Pause ();
		}
		
	}
}
