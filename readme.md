# Mars Rover Game

Built using Unity

 * Use directional arrows to navigate

Work in progress, was developed for class at UofT, INF2169: User Centred Information Systems Developed. The goal of the game is to teach school children about space and the international space station. This is a MVP which provides two levels, travelling through space and travelling on mars using the mars rover.